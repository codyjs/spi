package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	for true {
		scanner := bufio.NewScanner(bufio.NewReader(os.Stdin))
		fmt.Print("calc> ")
		res := scanner.Scan()

		if res {
			l := NewLexer(scanner.Text())
			p := NewParser(l)
			i := NewInterpreter(p)
			res := i.Interpret()

			fmt.Println(res)
		}

	}
}
