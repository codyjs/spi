package main

import (
	"fmt"
	"reflect"
)

type Interpreter struct {
	parser Parser
}

func NewInterpreter(p *Parser) *Interpreter {
	i := Interpreter{*p}
	return &i
}

func (i *Interpreter) Interpret() int {
	tree := i.parser.Parse()
	return i.Visit(tree)
}

func (i *Interpreter) Visit(astNode interface{}) int {
	switch n := astNode.(type) {
	case BinOpNode:
		return i.visitBinOpNode(n)
	case NumNode:
		return i.visitNumNode(n)
	case UnaryOpNode:
		return i.visitUnaryOpNode(n)
	default:
		panic(fmt.Sprintf("Unknown node type %s", reflect.TypeOf(n)))
	}
}

func (i *Interpreter) visitBinOpNode(node BinOpNode) int {
	switch node.op.tokenType {
	case PLUS:
		return i.Visit(node.left) + i.Visit(node.right)
	case MINUS:
		return i.Visit(node.left) - i.Visit(node.right)
	case MUL:
		return i.Visit(node.left) * i.Visit(node.right)
	case DIV:
		return i.Visit(node.left) / i.Visit(node.right)
	default:
		panic(fmt.Sprintf("Unknown BinOpNode token %s", node.op.tokenType))
	}
}

func (i *Interpreter) visitUnaryOpNode(node UnaryOpNode) int {
	switch node.op.tokenType {
	case PLUS:
		return +i.Visit(node.expr)
	case MINUS:
		return -i.Visit(node.expr)
	default:
		panic("Invalid unary operation")
	}
}

func (i *Interpreter) visitNumNode(node NumNode) int {
	return node.value
}
