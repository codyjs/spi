package main

import (
	"fmt"
	"strconv"
	"unicode"
)

const (
	ASSIGN  = "ASSIGN"
	SEMI    = "SEMI"
	ID      = "ID"
	DOT     = "DOT"
	INTEGER = "INTEGER"
	PLUS    = "PLUS"
	MINUS   = "MINUS"
	MUL     = "MUL"
	DIV     = "DIV"
	LPAREN  = "("
	RPAREN  = ")"
	EOF     = "EOF"
)

RESERVED_KEYWORDS = map[string]Token{
	"BEGIN": Token{"BEGIN", "BEGIN"},
	"END":   Token{"END", "END"},
}

type Token struct {
	tokenType string
	value     interface{}
}

type Lexer struct {
	text        string
	pos         int
	currentChar *rune
}

func NewLexer(text string) *Lexer {
	r := rune(text[0])
	l := Lexer{text, 0, &r}
	return &l
}

func (l *Lexer) Error() {
	panic(fmt.Sprintf("Invalid character %c", *l.currentChar))
}

func (l *Lexer) Peek() *rune {
	peekPos := l.pos + 1
	if peekPos > len(l.text)-1 {
		return nil
	}
	r := rune(l.text[peekPos])
	return &r
}

func (l *Lexer) _id() Token {
	res := ""
	for l.currentChar != nil && (unicode.IsLetter(*l.currentChar) || unicode.IsNumber(*l.currentChar)) {
		res += string(*l.currentChar)
		l.Advance()
	}

	t, prs := RESERVED_KEYWORDS[res]

	if !prs {
		t = Token{ID, res}
	}

	return t
}

func (l *Lexer) Advance() {
	l.pos++
	if l.pos > len(l.text)-1 {
		l.currentChar = nil
	} else {
		r := rune(l.text[l.pos])
		l.currentChar = &r
	}
}

func (l *Lexer) SkipWhitespace() {
	for l.currentChar != nil && unicode.IsSpace(*l.currentChar) {
		l.Advance()
	}
}

func (l *Lexer) Integer() int {
	result := ""
	for l.currentChar != nil && unicode.IsNumber(*l.currentChar) {
		result += string(*l.currentChar)
		l.Advance()
	}
	val, err := strconv.ParseInt(result, 10, 64)
	if err != nil {
		panic(fmt.Sprintf("Invalid integer %s", result))
	}

	return int(val)
}

func (l *Lexer) GetNextToken() Token {

	for l.currentChar != nil {

		if unicode.IsSpace(*l.currentChar) {
			l.SkipWhitespace()
		}

		if unicode.IsNumber(*l.currentChar) {
			return Token{INTEGER, l.Integer()}
		}

		if *l.currentChar == '+' {
			l.Advance()
			return Token{PLUS, '+'}
		}

		if *l.currentChar == '-' {
			l.Advance()
			return Token{MINUS, '-'}
		}

		if *l.currentChar == '*' {
			l.Advance()
			return Token{MUL, '*'}
		}

		if *l.currentChar == '/' {
			l.Advance()
			return Token{DIV, '/'}
		}

		if *l.currentChar == '(' {
			l.Advance()
			return Token{LPAREN, '('}
		}

		if *l.currentChar == ')' {
			l.Advance()
			return Token{RPAREN, ')'}
		}

		if unicode.IsLetter(*l.currentChar) {
			return l._id()
		}

		if *l.currentChar == ':' && *l.Peek() == '=' {
			l.Advance()
			l.Advance()
			return Token{ASSIGN, ":="}
		}

		l.Error()
	}

	return Token{EOF, nil}
}
