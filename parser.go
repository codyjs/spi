package main

import "fmt"

type Parser struct {
	lexer        Lexer
	currentToken Token
}

func NewParser(l *Lexer) *Parser {
	p := Parser{*l, l.GetNextToken()}
	return &p
}

func (p *Parser) Eat(tokenType string) {
	if p.currentToken.tokenType == tokenType {
		p.currentToken = p.lexer.GetNextToken()
	} else {
		panic(fmt.Sprintf("Unexpected token %s", p.currentToken.tokenType))
	}
}

func (p *Parser) Factor() interface{} {
	// factor: (PLUS | MINUS) factor | INTEGER | LPAREN expr RPAREN
	t := p.currentToken

	switch t.tokenType {
	case PLUS:
		p.Eat(PLUS)
		return UnaryOpNode{t, p.Factor()}
	case MINUS:
		p.Eat(MINUS)
		return UnaryOpNode{t, p.Factor()}
	case INTEGER:
		p.Eat(INTEGER)
		return NumNode{t.value.(int)}
	case LPAREN:
		p.Eat(LPAREN)
		res := p.Expr()
		p.Eat(RPAREN)
		return res
	default:
		panic(fmt.Sprintf("Unexpected token %s", t.tokenType))
	}
}

func (p *Parser) Term() interface{} {
	// term : factor ((MUL | DIV) factor)*
	node := p.Factor()

	for p.currentToken.tokenType == MUL || p.currentToken.tokenType == DIV {
		t := p.currentToken
		if t.tokenType == MUL {
			p.Eat(MUL)
		} else if t.tokenType == DIV {
			p.Eat(DIV)
		}

		node = BinOpNode{node, t, p.Factor()}
	}

	return node
}

func (p *Parser) Expr() interface{} {
	// expr : term ((PLUS | MINUS) term)*
	node := p.Term()

	for p.currentToken.tokenType == PLUS || p.currentToken.tokenType == MINUS {
		t := p.currentToken
		if t.tokenType == PLUS {
			p.Eat(PLUS)
		} else if t.tokenType == MINUS {
			p.Eat(MINUS)
		}

		node = BinOpNode{node, t, p.Term()}
	}

	return node
}

func (p *Parser) CompoundStatement() interface{} {
	// compound_statement : BEGIN statement_list END
	p.Eat(BEGIN)
	nodes = p.StatementList()
	p.Eat(END)
	return nodes
}

func (p *Parser) Program() interface{} {
	// program : compound_statement DOT
	node := p.CompoundStatement()
	p.Eat(DOT)
	return node
}

func (p *Parser) Parse() interface{} {
	return p.Expr()
}
