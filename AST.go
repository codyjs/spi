package main

type BinOpNode struct {
	left  interface{}
	op    Token
	right interface{}
}

type UnaryOpNode struct {
	op   Token
	expr interface{}
}

type CompoundStmtNode struct {
	children []interface{}
}

type AssignNode struct {
	left  interface{}
	op    Token
	right interface{}
}

type VarNode struct {
	token Token
	value interface{}
}

type NumNode struct {
	value int
}

type NoOpNode struct{}
